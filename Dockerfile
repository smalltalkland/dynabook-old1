FROM alpine
RUN wget https://github.com/ipfs/go-ipfs/releases/download/v0.6.0/go-ipfs_v0.6.0_linux-amd64.tar.gz
RUN tar -xvzf go-ipfs_v0.6.0_linux-amd64.tar.gz
RUN sudo bash /go-ipfs/install.sh > /etc/ipfs-install-output

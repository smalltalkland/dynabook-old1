class Rustable{

}
extern "C" void delete_rust(Rustable* val){
  delete val;
}
/*
_#include <taskflow/taskflow.hpp>  // Taskflow is header-only
tf::Taskflow taskflow;
void* create_emplace(void(fn)(void*),void* data){
  return &taskflow.emplace([](){fn(data);});
}
void* create_emplace_with_subflow(void(fn)(void*,void*),void* data){
  return &taskflow.emplace([](tf::Subflow& subflow){fn(data,subflow);});
}
void* create_emplace_in_subflow(void* subflow,void(fn)(void*),void* data){
  return &subflow.emplace([](){fn(data);});
}
void* create_emplace_bool(bool(fn)(void*),void* data){
  return &taskflow.emplace([](){return fn(data);});
}
void precede(void* a,void* b){
  a.precede(b);
}
void precede_bool(void* a,void* b,void* c){
  a.precede(b,c);
}
tf::Task createProcessor(tf::Task(child)(std::vector*),bool(getKilled)(),std::vector<std::vector<void*>>* ipcHooks){
  auto processor = taskflow.emplace([](tf::Subflow& subflow){
    std::vector<void*>* vec = new std::vector<void*>();
    auto val = child(vec);
    auto nxtask = subflow.emplace([](){
      while (!vec.empty())
      {
        auto val = vec.back();
        vec.pop_back();
        for(auto hook : ipcHooks){
          hook.push_back(val);
        }
      };
      return getKilled();});
    nxtask.precede(val,subflow.emplace([](){}));
    val.precede(nxtask);
  })
}
*/
extern "C" void* create_cpp_thread(void(fn)(void*),void* data){
  return new std::thread(fn,data);
}
extern "C" void delete_cpp_thread(void* thread){
  delete (std::thread*)thread
}

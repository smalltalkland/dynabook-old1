use crate::ondrop::*;
extern "C"{
    fn delete_cpp_thread(thread: *mut ()),
    fn create_cpp_thread(func: fn (*mut ()) -> (),data: *mut ()) -> *mut ()
}
pub fn create_thread(func: dyn FnMut () -> ()) -> (*mut (),ondrop){
    unsafe fn call(data: *mut ()){
        std::panic::catch_panics(move ||{
        std::mem::transmute<*mut(),dyn FnMut() -> ()>(data)();
    });
    };
    let thread = unsafe{create_cpp_thread(call,std::mem::transmute(func))};
    (thread,ondrop{
        ondrop: ||{
            unsafe{
                delete_cpp_thread(thread);
            }
        }
    });
}

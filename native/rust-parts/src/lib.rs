#[allow(mutable_transmutes)]
extern crate gc;
extern crate warp;
#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
pub mod heap;
pub mod socket;
pub mod cpp;
pub mod ondrop;

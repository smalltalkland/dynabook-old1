static let mut HTTP_SERVER = warp::filters::sse::event("disable");
pub unsafe fn add_route(route: String,target: fn (*mut (),String) -> String,data: *mut ()){
    HTTP_SERVER = HTTP_SERVER.or(warp::path(route).and(warp::path::param()).map(move |param|{target(data,param)}))
}

pub struct ondrop{
    pub ondrop: FnOnce () -> ()
}
pub impl Drop for ondrop{
    fn drop(&mut self){self.ondrop();}
}

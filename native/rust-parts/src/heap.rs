use gc::*;
use std::vec::*;
pub enum SqObject{
SmallInteger (*mut SqObject,u64),
Composite (Option<*mut SqObject>,Vec<Option<*mut SqObject>>,Option<*mut SqObject>),
Memory (*mut SqObject,u64),
Boundary (SqObject),
}
pub impl Trace for SqObject{
    custom_trace!(self2,{
        match self2{
            SqObject::Composite(_,v,_) => {
                for i in 0..v.len(){
                    mark(sq_inst_var_at(self2),i)
                }
            },
            SqObject::Boundary(_) => {},
            _ => {}
        };
        match sq_class_nonmut(self2){
            Some(cls) => mark(cls),
            None => {}
        }
    })
}
pub fn sq_class(obj: &mut SqObject) -> Option<&mut SqObject>{
    (match obj{
        SqObject::SmallInteger(ptr,_) => Some(ptr),
        SqObject::Boundary(obj1) => sq_class(obj1).map(|x|{unsafe{std::mem::transmute(x)}})
        SqObject::Memory(ptr,_) => Some(ptr),
        SqObject::Composite(Some(cls),_,None) => Some(cls),
        SqObject::Composite(None,_,None) => None,
        SqObject::Composite(clso,_,Some(parent)) => (match unsafe{sq_class(std::mem::transmute(parent))} {
            Some(obj) => Some(obj),
            None => (match clso{
                Some(two) => Some(*two ),
                None => None,
            }).map(|x|{unsafe{std::mem::transmute(x)}})
        }).map(|x|{unsafe{std::mem::transmute(x)}})
    }).map(|x|{unsafe{std::mem::transmute(x)}})
}
pub fn sq_class_nonmut(obj: &SqObject) -> Option<&SqObject>{
    (match obj{
        SqObject::SmallInteger(ptr,_) => Some(ptr),
        SqObject::Boundary(obj1) => sq_class_nonmut(obj1).map(|x|{unsafe{std::mem::transmute(x)}})
        SqObject::Memory(ptr,_) => Some(ptr),
        SqObject::Composite(Some(cls),_,None) => Some(cls),
        SqObject::Composite(None,_,None) => None,
        SqObject::Composite(clso,_,Some(parent)) => (match unsafe{sq_class_nonmut(std::mem::transmute(parent))} {
            Some(obj) => Some(obj),
            None => (match clso{
                Some(two) => Some(*two ),
                None => None,
            }).map(|x|{unsafe{std::mem::transmute(x)}})
        }).map(|x|{unsafe{std::mem::transmute(x)}})
    }).map(|x|{unsafe{std::mem::transmute(x)}})
}
pub fn sq_inst_var_at(obj: &SqObject,key: usize) -> Option<&SqObject>{
    match obj{
        SqObject::SmallInteger(_,_) => None,
        SqObject::Memory(_,_) => None,
        SqObject::Composite(_,it,None) => it[key].map(|x|{unsafe{std::mem::transmute(x)}}),
        SqObject::Composite(_,it,Some(parent)) => match it[key]{
            Some(x) => unsafe{std::mem::transmute(x)},
            None => sq_inst_var_at(unsafe{std::mem::transmute(parent)}, key)
        }
    }
}
pub fn sq_inst_var_put(obj: &mut SqObject,key: usize,value: &mut SqObject){
    match obj{
        SqObject::SmallInteger(_,_) => {},
        SqObject::Memory(_,_) => {}
        SqObject::Composite(_,it,_) => {it[key] = unsafe{Some(value as *mut SqObject)}},
    }
}
pub fn sq_get_value(obj: &SqObject) -> Option<u64>{
    match obj{
        SqObject::SmallInteger(_,n) => Some(*n),
        SqObject::Memory(_,n) => Some(unsafe{*(*n as *mut u64)}),
        _ => None,
    }
}
pub fn sq_set_value(obj: &mut SqObject,val: u64){
    match obj{
        SqObject::SmallInteger(_,n) => {*n = val},
        SqObject::Memory(_,n) => {unsafe{*(n as *mut u64) = val}},
        _ => {},
    }
}

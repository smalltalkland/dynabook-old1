let bindThis = f => function(...args){return f(this,...args)}
export default () => {
    let handles = {};
    let getKey = (h1,h2) => (r => {handles[r] = handles[h1][handles[h2]]; return r})(Math.random());
    let setKey = (h1,h2,h3) => {handles[h1][handles[h2]] = handles[h3]};
    let toInt = (h1) => handles[h1]|0;
    let fromInt = i => {let r = Math.random(); handles[r] = i|0; return r};
    let callKey = (h1,h2) => (r => {handles[r] = handles[h1].call(...handles[h2])})(Math.random());
    let oneArray = (h1) => {let r = Math.random(); handles[r] = [handles[h1]]; return r}
    let twoArray = (h1,h2) => {let r = Math.random(); handles[r] = [handles[h1],handles[h2]]; return r}
    let baseHandly = bindThis(self => {let r = Math.random(); handles[r] = self; return r});
    let stringCreate = (...args) => {let r = Math.random(); handles[r] = String.fromCodePoint(...args); return r}
    let forwardHook = (func,table) => {let r = Math.random(); handles[r] = bindThis((self,...args) => func.call(table,self,...args)); return r}
    function HandlyLib(stdlib,ffi,heap){
        "use asm";
        return {}
    }
    let hffi = {getKey,setKey,toInt,fromInt,callKey,oneArray,twoArray,baseHandly,stringCreate,forwardHook};
    let hlib = HandlyLib(window,hffi,new ArrayBuffer(0x100000));
    return new Proxy(hffi,{get: bindThis((self,o,k) => (o[k] || hlib[k]).bind(self)),has: (o,k) => k in o || k in hlib,ownKeys: o => Reflect.ownKeys(o).concat(Reflect.ownKeys(hlib))});
}
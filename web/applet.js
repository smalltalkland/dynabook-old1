export function applet(code){
let appletCtxt = new Proxy({WebAssembly: new Proxy(WebAssembly,{getPrototypeOf: () => null}),Math: new Proxy(Math,{getPrototypeOf: () => null})},{has: () => true,get: (o,k) => k === Symbol.unscopables ? undefined : o[k]});
new Function('ctxt','code','with(ctxt)return eval(code)')(appletCtxt,code);
}
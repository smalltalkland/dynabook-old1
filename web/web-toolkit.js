import $ from 'https://jspm.dev/cash-dom'
import {bind,wire} from 'https://jspm.dev/hyperhtml'
import {Left,Right,Some,None,Return,Suspend} from 'htps://jspm.dev/monet'
import L from 'https://jspm.dev/leaflet'
import Croquet from 'https://jspm.dev/@croquet/croquet'

import protector from '../obf/protect.js'
let bindThis = f => function(...args){return f(this,...args)}
let allParents = n => {
  var a = n;
var els = [];
while (a) {
    els.unshift(a);
    a = a.parentNode;
};
return n;
}
let toolkit = {};
$.fn.tk = bindThis(base => Object.assign(Object.create(toolkit),{base}));
toolkit.basicButton = bindThis(({base},handler) => base.on('click',evt => (() => {try{return top.document.domain == document.domain}catch(err){return false;}})() ? handler(evt) : base.st_cjp || confirm('Your page might be clickjacked :) say yes to allow') ? base.st_cjp = true && handler(evt) : null));
toolkit.checkbox = bindThis(({base},{get,set,styler}) => base.tk().basicButton(_ => set(!get())).css(styler(get)));
toolkit.button = bindThis(({base},{onClick,styler}) => base.tk().basicButton(onClick).css(styler()));
toolkit.basicTextField = bindThis(({base},{onChange}) => base.each(x => new MutationObserver(ml => onChange(ml.map(m => ({target: $(m.target),addedNodes: m.addedNodes.map($),removedNodes: m.removedNodes.map($)})))).observe(x,{subtree: true,childNodes: true})).each(x => {x.cntentEditable = true; return x}));
toolkit.plainTextField = bindThis(({base},{get,set}) => base.tk().basicTextField(ml => {set(base.text()); base.text(get())}))
export let htk = new Proxy(Object.create($.fn.tk.call($.fn)),{get: (o,k) => (c,...args) => (h => o[k].call($.fn.tk.call($(h )),...args) && h)(wire([c,...args])`<span>${c}</span>`),set: (o,k,v) => {}})
export let cetk = new Proxy(htk,{get: (o,k) => (old => class extends HTMLElement{
    setAttribute(k,...args){
        if(k === 'props')this.props = args[0];
        super.setAttribute(k,...args);
    }
    getAttribute(k,...args){
        if(k === 'props')return this.props;
        return super.getAttribute(k,...args);
    }
    constructor(...args) {
      super(...args);
      this.html = bind(this);
    }
    attributeChangedCallback() { this.render(); }
    connectedCallback() { this.render(); }
    render() {
        if(!this.props)return this.html``;
      return this.html`<span>${old(...this.props)}</span>`
    }
})(o[k])})
toolkit.overlay = bindThis(({base},{background}) => base.append(background));
toolkit.window = bindThis(({base},{onClose,onMenu}) => base.each(n => allParents(n).filter(p => p.wm.isSome()).forEach(wm => wm.wm.onWindow(n)) && bind(n.shadowRoot || n.attachShadow({mode: 'open'}))`<div><div>${htk.basicButton(wire(onClose)`<button>X</button>`,onClose)}<slot name = "title"></slot>${htk.basicButton(wire(onMenu)`<button>v</button>`,onMenu)}</div><slot></slot></div>`))
toolkit.windowManager = bindThis(({base},{onWindow}) => base.each(n => n.wm = Some({onWindow})))
HTMLElement.prototype.wm = None;
toolkit.map = bindThis(({base},{}) => base.each(e => (m => {L.tileLayer(toolkit.map.settings.url,{...toolkit.map.settings.layerProps}).addTo(m); e.__map = m; $(e).tk().basicTextField(ml => {ml.addedNodes.each(n => (n.mapElement = n.getInitialMapElement || L.popup().setContent(n)).addTo(m )); ml.removedNodes.each(n => n.mapElement.remove() && (n.mapElement = null))})})(L.map(e.shadowRoot || e.attachShadow({mode: 'open'})))));
export let mapSettings = toolkit.map.settings = {};
let getHoveredElement = ((getNodes,contains) => pos => new Proxy(getNodes(document.body).filter(n => contains(n,pos)),{get: (o,k) => o[k] || o[0][k] instanceof Function ? o[0][k].bind(o[0]) : o[0][k]}))(function(node){
  // do some thing with the node here
  var nodes = node.childNodes;
  for (var i = 0; i <nodes.length; i++){
      if(!nodes[i]){
          continue;
      }

      if(nodes[i].childNodes.length > 0){
          loop(nodes[i]);
      }
      if(nodes[i].shadowRoot){
        loop(nodes[i].shadowRoot)
      }
  }
},(n,pos) => {
  let domRect = n.getBoundingClientRect();
  domRect.contains = function (x, y) {
    return this.x <= x && x <= this.x + this.width &&
           this.y <= y && y <= this.y + this.height;
};
return domRect.contains(pos.left,pos.top);
})
export class HandMorphModel extends Croquet.Model{
  init(options={}){
    this.xy = {x: 0,y: 0}
  }
}
export let defaultHand = props => {
return bind(document.body)`<div class = ${`hand default-hand`} onload = ${evt => {document.defaultHand = evt.target}} onmousemove = ${evt => {let rect = document.defaultHand.getBoundingClientRect();document.defaultHand.style = {left: evt.pageX + (rect.width / 2),top: evt.pageY + (rect.height / 2)}; evt.target.onclick(evt)}} onclick = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)} oncontextmenu = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)}
onmousedown = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)} onmouseup = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)} onkeypress = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)} onkeydown = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)} onkeyup = ${evt => getHoveredElement(document.defaultHand.style).dispatchEvent(evt)}
></div>`
}
toolkit.protect = bindThis(({base},{}) => base.tk().basicTextField(protector(base,'tf')).each(n => $(n.parentNode).tk().basicTextField(protector($(n.parentNode),'ptf',base))))
import {ieInPractice, set_ieInPractice} from './data.js'
export let ieMode = false;
export function set_ieMode(v){ieMode = v}
(ftable => [Function,Array,Object,String,Symbol,Promise].forEach(c => c.prototype = new Proxy(c.prototype,{get: (o,k) => ieInPractice && ieMode ? ftable(c,k,o) : o[k]})))((c,k,o) => {
    let nf = o[k];
    if(c === Symbol || c === Promise){
        if(k === 'constructor')nf = function(){throw new TypeError(`${c.toString()} is undefined`)};
        return nf;
    };
    return nf;
})
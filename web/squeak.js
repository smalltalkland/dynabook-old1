import 'https://raw.githubusercontent.com/codefrau/SqueakJS/main/squeak.js'
import $ from 'https://jspm.dev/cash-dom'
import sqAsm from './sqAsmPlugin.js'
Squeak.registerExternalModule('SqueakAssemblyPlugin',sqAsm);
export function runSqueak(...args){
    return new Promise(c => {
        let p = Object.getOwnPropertyDescriptor(SqueakJS,'vm');
        Object.defineProperty(SqueakJS,'vm',{get: () => null,set: v => {
            c(v);
            Object.defineProperty(SqueakJS,'vm',p);
        }});
        SqueakJS.runSqueak(...args);
    })
}

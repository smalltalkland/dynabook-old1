import * as fs from 'https://deno.land/std/fs/mod.ts'
import avos from 'https://gitlab.com/smalltalkland/dynabook/-/raw/master/web/avos.js'
let deno = async (path,onStreams) => {let read = await readFileStr(path); let theJS = read; let new_ = `data:application/javascript;base64,${btoa(theJS)}`;let p = Deno.run({cmd: ['deno','run',new_],stdout: 'piped',stderr: 'piped',stdin: 'piped'}); let rx = ((o,r) => new Proxy(r,{get: (oo,k) => k === 'operators' ? o : oo[k]})))(await import('https://jspm.dev/rxjs.operators'),await import('https://jspm.dev/rxjs'));let streams = new Proxy(p,{get: (o,k) => new Proxy(new rx.Observable(async s => {let reader = o[k].getReader();       function push() {
        // "done" is a Boolean and value a "Uint8Array"
        reader.read().then(({ done, value }) => {
          // Is there no more data to read?
          if (done) {
            // Tell the browser that we have finished sending data
            s.complete();
            return;
          }

          // Get the data and send it to the browser via the controller
          s.next(value);
          push();
        });
      };

      push();
    }),{apply: (o2,t,args) => (w => (w.write(args[0]) || 2) && w.releaseLock())(o[k].getWriter())})
await onStreams(streams);
  })}
let infoServer = () => {
  Deno.run({cmd: ['deno','run','--allow-net','--allow-read','--allow-write','https://gitlab.com/smalltalkland/dynabook/-/raw/master/web/server.js']});
}

export let MonadJS = m => f => MonadJS(m.bind(f));

export let True = x => y => x;
export let False = x => y => y;
export let Zero = False;
export let One = f => x => f(x);
export let Nultiply = f1 => f2 => x => f1(f2(x))
export let Pair = a => b => f => f(a)(b)

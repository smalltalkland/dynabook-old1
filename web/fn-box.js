let bindThis = f => function(...args){return f(this,...args)}
let call = Function.prototype.call.call.bind(Function.prototype.call);
let box = it => (s => new Proxy(it,new Proxy({},{get: (o,k) => bindThis((self,...args) => k === 'get' && args[1] === s ? it.box : k === 'set' && args[1] === s ? it.box = args[2] : call(it.box ? (() => {}) : o[k],self,...args))})))(Symbol())
[Function,Object,Array].forEach(c => c.prototype = box(c.prototype))
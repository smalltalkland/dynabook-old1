import './ctxt.js'
import * as utils from './utils.js'
import { serve } from "https://deno.land/std@0.50.0/http/server.ts";
import { serveFile } from "https://deno.land/std/http/file_server.ts";
import {extname} from 'https://deno.land/std/path/mod.ts'
import private from './privacy.js'
import Ipfs from './ipfs.js'
const PORT = Deno.env.get("PORT") || 8000;
const s = serve({ port: PORT });
const MEDIA_TYPES = {
    ".md": "text/markdown",
    ".html": "text/html",
    ".htm": "text/html",
    ".json": "application/json",
    ".map": "application/json",
    ".txt": "text/plain",
    ".ts": "text/typescript",
    ".tsx": "text/tsx",
    ".js": "application/javascript",
    ".jsx": "text/jsx",
    ".gz": "application/gzip",
    ".css": "text/css",
    ".wasm": "application/wasm",
  };

  /** Returns the content-type based on the extension of a path. */
  function contentType(path){
    return MEDIA_TYPES[extname(path)];
  }


async function serveFileStr(
    req,
    filePath,
    theString,
  ){
    const headers = new Headers();
    headers.set("content-length", theString.length.toString());
    const contentTypeValue = contentType(filePath);
    if (contentTypeValue) {
      headers.set("content-type", contentTypeValue);
    }
    return {
      status: 200,
      body: theString,
      headers,
    };
  }


console.log(`http://localhost:${PORT}/`);
  let server_node = await Ipfs.create();
for await (const req of s) {
    try{
  const response = req.url.startsWith('https') ? await serveFileStr(req,await Deno.realPath('./' + req.url),await (async d => req.url.includes('obf') ? await private(d) : d)(await (await fetch(req.url)).text())) : req.url.startsWith('obf') ? await serveFileStr(req,await Deno.realPath('./' + req.url),private(await Deno.realPath('./' + req.url))) : await serveFile(req, await Deno.realPath('./' + req.url));
  response.headers.set("x-powered-by", "express");
  req.respond(response);
    }catch(err){
        if(req.url.startsWith('res/')){
            let u = req.url.slice(4);
            const response = await (await import('./' + u))();
  response.headers.set("x-powered-by", "express");
  req.respond(response);
}else if(req.url.startsWith('account/')){
  let u = req.url.slice(8);
let user = u.split('/').pop();
let action = (a => {a.pop(); return a.join('/')})(u.split('/'));
let query = action.split('?')[1];
action = action.split('?')[0];
let userModule = await import('./usr/' + action);
let response = await userModule(user,query);
if(response.headers)response.headers.set("x-powered-by", "express");
req.respond(response);
}
    }
}

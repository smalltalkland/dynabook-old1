import {addUser} from '../../mysql_js/storage.js'
export default async (_,query) => {
  let parsed = JSON.parse(query);
  let [userData,id] = await addUser(parsed.firstName,parsed.lastName,parsed.age);
  return {body: JSON.stringify({...userData,id})}
}

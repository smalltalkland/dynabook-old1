import handly from './handly.js'
let theHandly = handly();
export default {
    getModuleName: function() { return "SqueakAssemblyPlugin"; },
    interpreterProxy: null,

    setInterpreter: function(anInterpreter) {
      this.interpreterProxy = anInterpreter;
      return true;
    },
    modules: {},
    startModule(moduleID){
        let module = this.modules[moduleID];
        function TheASMModule(stdlib,ffi,HEAP_){
            "use asm";
            var HEAP = new stdlib.Int32Array(HEAP_)
        }
        TheASMModule(self,{...theHandly,...module},new ArrayBuffer(0x100000));
    }
}
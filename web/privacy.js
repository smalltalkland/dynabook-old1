import {obfuscate} from 'https://jspm.dev/javascript-obfuscator'
import {readFileStr} from 'https://deno.land/std/fs/mod.ts'
export default async f => {
let fs = await readFileStr(f);
let o = obfuscate(fs,    {
    compact: false,
    controlFlowFlattening: true,
    numbersToExpressions: true,
    simplify: true,
    shuffleStringArray: true,
    splitStrings: true,
    domainLock: ["localhost","localhost:8000","dynabook.it"],
}).toString();
let r = /^import .*$/;
let i = o.match(r);
o = o.replace(r,'');
return i.reduce((x,y) => x + y) + o
}
import './ctxt.js'
import './fn-box.js'
import * as utils from './utils.js'
//import {ieMode,set_ieMode} from './ieSupport.js'
import {htk} from './web-toolkit.js'
import {bind,wire} from 'https://jspm.dev/hyperhtml'
import {aac} from './js-aac.js'
//import {runSqueak} from './squeak.js'
import $ from 'https://jspm.dev/cash-dom'
self.onload = () => {
    /*
$(document.querySelector('#settings')).tk().basicButton(evt => {
    $(document.querySelector('#settingsView')).toggle();
});
*/
bind(document.body)`${htk.basicButton(wire()`<button id = "settings">Settings</button>`,evt => {
    $(document.querySelector('#settingsView')).toggle();
})}
<div id = "settingsView" style = "display: none">
    <!--<button class = "ieMode"></button>-->
</div>`
//$('#settingsView .ieMode').tk().checkbox({get: () => ieMode,set: v => {
    //set_ieMode(v);
//}})
}

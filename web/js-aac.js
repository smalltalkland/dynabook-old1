import {bind,wire} from 'https://jspm.dev/hyperhtml'
import {htk} from './web-toolkit.js'
export async function aac(data){
    let completed = false;
    let action = 'yes';
    let elem = document.createElement('div');
    bind(elem)`<div>
    ${data}
    ${htk.basicButton(wire()`Yes`,() => {
        completed = true;
    })}
    ${htk.basicButton(wire()`No`,() => {
        completed = true;
        action = 'no';
    })}
    </div>`;
    document.body.appendChild(elem);
    while(!completed)await new Promise(requestAnimationFrame);
    document.body.removeChild(elem);
    if(action === 'no')throw new Error('no');
}
HTMLElement.prototype.dispatchEvent = (super_ => async function(evt){
    if(!evt instanceof CustomEvent)await aac(wire(evt)`An attacker is typing to become you by doing a ${evt.type}!! ${htk.basicButton(wire()`More...`,() => {alert({...evt})})}`);
    return super_.call(this,evt);
})(HTMLElement.prototype.dispatchEvent)

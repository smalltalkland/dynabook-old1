import {aac} from '../web/js-aac.js'
let enclave = {};
enclave = new Proxy(enclave,{set: (o,k,v) => aac(`Are you trying to set key ${k} of the secure enclave?`).then(() => o[k] = v),delete: () => {}})
export default enclave
import '../../../web/ctxt.js'
import { botCache } from "../../mod.ts";
botCache.commands.set('evalJS',message => {
    let f;
    with(new Proxy({Object: new Proxy(Object,{get: (o,k) => k === 'prototype' ? null : k,getPrototypeOf: () => null}),Reflect: new Proxy(Reflect,{getPrototypeOf: () => null}),Proxy: new Proxy(Proxy,{getPrototypeOf: () => null})},{has: () => true}))f = eval_ => x => eval_(x);
    message.channel.sendMessage(f(eval)(message.content));
})